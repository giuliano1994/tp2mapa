package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import negocio.ArbolGeneradorMinimo;
import negocio.Grafo;

public class ArbolGenMinimoTest {

	
	private ArbolGeneradorMinimo arb1;
	private ArbolGeneradorMinimo arb2;
	
	private Grafo g;
	private Grafo h;
	private Grafo r;
	Set<String> esperados;
	
	
	@Before
	public void contruirArbol_Grafo() {
		
		g = new Grafo();
		h = new Grafo();
		r = new Grafo();
		
		g.agregarArista("a", "b", 2);
		g.agregarArista("a", "c", 3);
		g.agregarArista("b", "c", 5);
		g.agregarArista("a", "d", 3);
		g.agregarArista("d", "c", 1);
		g.agregarArista("d", "b", 4);
		
		arb1 = new ArbolGeneradorMinimo(g);
		
		esperados = new HashSet<String>();
		
		esperados.add("a");
		esperados.add("b");
		esperados.add("c");
		esperados.add("d");
		
		h.agregarVertice("a");
		h.agregarVertice("b");
		h.agregarVertice("c");
		h.agregarVertice("d");
		h.agregarArista("a", "b", 2);
		h.agregarArista("a", "c", 3);
		h.agregarArista("c", "d", 1);
		
		r.agregarArista("a", "b", 2);
		r.agregarArista("a", "c", 3);
		r.agregarArista("b", "c", 5);
		r.agregarArista("a", "d", 3);
		r.agregarArista("d", "c", 1);
		r.agregarArista("ff", "hh", 4);
		
		arb2 = new ArbolGeneradorMinimo(r);
		
	}
	
	
	
	@Test
	public void recorrerGrafoTest() {

		g = arb1.iniciar();
		
		Assert.igualesAGM(esperados, g.getCiudades());
	
	}

	
	@Test
	public void recorrerRelacionesIgualesTest() {
		
		g = arb1.iniciar();
		
		Assert.relacionesIguales(g, h);
		
	}
	
	
	@Test
	public void tamaņoTest() {
		
		g = arb1.iniciar();
		
		assertEquals(4, g.tamanio());
		
	}
	
	
	
	@Test (expected = IllegalArgumentException.class)
	public void grafoNoConexoTest() {
		
		r = arb2.iniciar();
		
	}
	
	
	
}
