package tests;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import negocio.BFS;
import negocio.Grafo;

public class BFSTest {
	
	private Grafo g;
	private Grafo h;
	private BFS bfs1;
	private BFS bfs2;
	HashMap<String, Boolean> lista;
	HashMap<String, Boolean> lista2;
	
	
	@Before
	public void contruirArbol_Grafo() {
		
		g = new Grafo();
		h = new Grafo();
		bfs1 = new BFS(g);
		bfs2 = new BFS(h);
		lista = new HashMap<String, Boolean>();
		lista2 = new HashMap<String, Boolean>();
		
		g.agregarArista("a", "b", 2);
		g.agregarArista("a", "c", 3);
		g.agregarArista("b", "c", 5);
		g.agregarArista("a", "d", 3);
		g.agregarArista("d", "c", 1);
		g.agregarArista("d", "b", 4);
	//	g.agregarArista("ff", "hh", 2);
	
			
		h.agregarArista("a", "b", 2);
		h.agregarArista("a", "c", 3);
		h.agregarArista("b", "c", 5);
		h.agregarArista("a", "d", 3);
		h.agregarArista("d", "c", 1);
		h.agregarArista("d", "b", 4);
		h.agregarArista("ff", "hh", 2);

		
	}
	

	@Test
	public void esConexoTest() {
		
		Assert.bfsConexoTest(lista);
		
		assertTrue(bfs1.recorrerGrafo("a"));

	}
	
	
	@Test
	public void esNoConexoTest() {
		
		assertFalse(bfs2.recorrerGrafo("a"));

	}
	
	@Test
	public void tamaņoTest() {
		
		int tamanioAnterior = g.tamanio();
		
		bfs1.recorrerGrafo("a");
		
		assertEquals(tamanioAnterior, g.tamanio());
	}
	
	
	

}
