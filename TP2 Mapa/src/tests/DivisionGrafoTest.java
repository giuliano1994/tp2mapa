package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;

import negocio.DivisionGrafo;
import negocio.Grafo;

public class DivisionGrafoTest {
	
	private Grafo g;
	private Grafo h;
	private Grafo i;
	private DivisionGrafo div;

	
	@Before
	public void contruirArbol_Grafo() {
		
		g = new Grafo();
		h = new Grafo();
		div = new DivisionGrafo();
		
		g.agregarArista("a", "b", 2);
		g.agregarArista("a", "c", 3);
		g.agregarArista("b", "c", 5);
		g.agregarArista("a", "d", 3);
		g.agregarArista("d", "c", 1);
		g.agregarArista("d", "b", 4);
		
		h.agregarArista("a", "b", 2);
		h.agregarArista("a", "c", 3);
		h.agregarArista("a", "d", 3);
		h.agregarArista("d", "c", 1);
		h.agregarArista("d", "b", 4);
		
	}

	@Test
	public void eliminarRelacionMasCostosaTest() {
		
		g = div.iniciar(g);
		
		Assert.DivisionSonIguales(h, g);

	}
	
	
	@Test
	public void tamaņosIgualesTest() {
		
		g = div.iniciar(g);
		
		assertEquals(h.tamanio(), g.tamanio());
				
	}
	
	
	@Test (expected = IllegalArgumentException.class)
	public void grafoVacioTest() {
		
		i = div.iniciar(i);
		
	}
	

}
