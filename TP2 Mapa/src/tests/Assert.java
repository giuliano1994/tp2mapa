package tests;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import negocio.Grafo;

public class Assert
{

	public static void iguales(String[] esperados, Set<String> obtenidos) 
	{
		
		for (int i = 0 ; i < esperados.length; i++) {
			
				assertTrue(obtenidos.contains(esperados[i]));
		}
	}
	
	

	
	public static void igualesAGM(Set<String> esperados, Set<String> obtenidos) 
	{
		
		for (String ver: obtenidos) {
				
				System.out.println("ingreso con " + ver);
			
				assertTrue(esperados.contains(ver));

		}
	}

	
	
	
	public static void relacionesIguales(Grafo esperados, Grafo obtenidos) 
	{
		
		for (String verEsperoado : esperados.getCiudades()) {
		
			for (String ver : obtenidos.getCiudades()) {
		
				for (Entry<String, Integer> ari : obtenidos.recorrerAristas2(ver)) {
			
					if (verEsperoado.compareTo(ver) == 0) {
			
						assertTrue(esperados.recorrerAristas2(verEsperoado).contains(ari));
					}
					
				}
			}
		}
	}
	
	
	
	public static void bfsConexoTest(HashMap<String, Boolean> h) {
		
		boolean resultado = true;
		
		for (Entry<String, Boolean> ver : h.entrySet()) {
				
			if (!ver.getValue()) {
				
				resultado =false;
			}

		}
		
		assertTrue(resultado);
	}
	
	
	
	public static void bfsNOConexoTest(HashMap<String, Boolean> i) {
		
		boolean resultado = true;
		
		for (Entry<String, Boolean> ver : i.entrySet()) {
				
			if (!ver.getValue()) {
				
				resultado =false;
			}

		}
		
		assertFalse(resultado);
	}
	
	
	
	public static void DivisionSonIguales (Grafo esperado, Grafo obtenido) {
		
		
		for (String verEsperoado : esperado.getCiudades()) {
			
			for (String ver : obtenido.getCiudades()) {
			
				for (Entry<String, Integer> ari : obtenido.recorrerAristas2(ver)) {
				
					if (verEsperoado.compareTo(ver) == 0) {
				
						assertTrue(esperado.recorrerAristas2(verEsperoado).contains(ari));
					}
						
				}
			}
		}		
	
	}
	
	
	
	
}