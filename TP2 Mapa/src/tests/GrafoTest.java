package tests;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

import negocio.Grafo;

public class GrafoTest 
{

	//agregarArista(String, String, Integer)
	//EXCEPCIONES
	
	@Test (expected = IllegalArgumentException.class)
	public void cadenaVaciaTest() 
	{
		Grafo grafo = new Grafo();
		
		grafo.agregarVertice("");
	}
	

	@Test (expected = IllegalArgumentException.class)
	public void agregarLoopTest()
	{
		Grafo grafo = new Grafo();
		
		grafo.agregarArista("Bs. As", "Bs. As" , 5);
		
		
	}
	
	//HAPPY PATH
	@Test 
	public void agregarAristaTest()
	{
		Grafo grafo = new Grafo();
		grafo.agregarVertice("Bs. As");
		grafo.agregarVertice("Cordoba");
		
		grafo.agregarArista("Bs. As", "Cordoba" , 5);
		
		assertTrue(grafo.existeVertice("Bs. As"));
	}
	
	public void agregarAristaOpuestaTest()
	{
		Grafo grafo = new Grafo();
		
		grafo.agregarArista("Bs. As", "Cordoba" , 5);
		
		assertTrue(grafo.existeVertice("Cordoba"));
	}
	
	@Test 
	public void agregarAristaDosVecesTest()
	{
		Grafo grafo = new Grafo();
		grafo.agregarVertice("Bs. As");
		grafo.agregarVertice("Cordoba");
		
		grafo.agregarArista("Bs. As", "Cordoba" , 5);
		grafo.agregarArista("Bs. As", "Cordoba" , 5);
		
		assertTrue(grafo.existeVertice("Cordoba"));
	}
	
	
	@Test
	public void reemplazarAristaTest()
	{
		Grafo grafo = new Grafo();
		grafo.agregarVertice("Bs. As");
		grafo.agregarVertice("Cordoba");
		
		grafo.agregarArista("Bs. As", "Cordoba", 5);
		grafo.agregarArista("Bs. As", "Cordoba" , 6);
		
		assertFalse(5==grafo.getSimilaridad("Bs. As", "Cordoba"));
	}
	
	//AgregarVertice(String)
	//EXCEPCIONES
	
	
	//HAPPY PATH
	
	@Test
	public void AgregarVerticeTest()
	{
		Grafo grafo = new Grafo();
		
		grafo.agregarVertice("Cordoba");
		
		assertTrue(grafo.existeVertice("Cordoba"));
	}
	
	@Test
	public void AgregarVerticeDosVecesTest()
	{
		Grafo grafo = new Grafo();
		
		grafo.agregarVertice("Cordoba");
		grafo.agregarVertice("Cordoba");
		
		assertEquals(1, grafo.tamanio());
	}
	
	
	//eliminarArista
	//EXCEPCIONES
	 @Test(expected = IllegalArgumentException.class)
	 public void eliminarAristaInexistenteTest()
	 {
		 Grafo g=new Grafo();
		 
		 g.eliminarArista("Bs As", "Chaco");
	 }
	 
	 @Test (expected = IllegalArgumentException.class)
	 public void eliminarAristaDosVecesTest()
	 {
		 Grafo g= new Grafo();
		 g.agregarVertice("Bs As");
		 g.agregarVertice("Cordoba");
		 
		 g.agregarArista("Bs As", "Cordoba", 0);
		 g.eliminarArista("Bs As", "Cordoba");
		 g.eliminarArista("Bs As", "Cordoba");
	 }
	 
	 //HAPPY PATH
	 @Test 
	 public void eliminarAristaExistenteTest()
	 {
		 Grafo g= new Grafo();
		 g.agregarVertice("San Juan");
		 g.agregarVertice("Santa Fe");
		 
		 g.agregarArista("San Juan", "Santa Fe", 3);
		 g.eliminarArista("San Juan", "Santa Fe");
		 
		 assertFalse(g.existeArista("San Juan", "Santa Fe"));
	 }
	 
	 @Test
	 public void eliminarAristaInversaTest()
	 {
		 Grafo g=new Grafo();
		 
		 g.agregarVertice("Mendoza");
		 g.agregarVertice("Jujuy");
		 
		 g.agregarArista("Mendoza", "Jujuy", 5);
		 g.eliminarArista("Jujuy", "Mendoza");
		 
		 assertFalse(g.existeArista("Mendoza", "Jujuy"));
	 }
	 
	 //eliminarVertice
	 //EXCEPCIONES
	 @Test (expected = NullPointerException.class)
	 public void eliminarVerticeInexistenteTest()
	 {
		 Grafo g= new Grafo();
		 
		 g.eliminarVertice("Jujuy");
	 }
	 
	 @Test (expected = NullPointerException.class)
	 public void eliminarVerticeDosVecesTest()
	 {
		 Grafo g=new Grafo();
		 
		 g.agregarVertice("Formosa");
		 g.eliminarVertice("Formosa");
		 g.eliminarVertice("Formosa");
	 }
	 
	 //HAPPY PATH
	 @Test
	 public void eliminarVerticeTest()
	 {
		 Grafo g=new Grafo();
		 g.agregarVertice("Tierra del Fuego");
		 g.agregarVertice("Jujuy");
		 
		 g.agregarArista("Tierra del Fuego", "Jujuy", 10);
		 g.eliminarVertice("Tierra del Fuego");
		 
		 assertEquals(1, g.tamanio());
	 }
	 
	 @Test
	 public void comprobarRelacionVerticeEliminadoTest()
	 {
		 Grafo g=new Grafo();
		 g.agregarVertice("Tierra del Fuego");
		 g.agregarVertice("Jujuy");
		 
		 g.agregarArista("Tierra del Fuego", "Jujuy", 10);
		 g.eliminarVertice("Tierra del Fuego");
		 
		 assertFalse(g.existeArista("Jujuy", "Tierra del Fuego"));
	 }
	 
	 
	 //getCiudades
	//HAPPY PATH
	 public void ciudadesTest()
	 {
		 Grafo g=new Grafo();
		 g.agregarVertice("Cordoba");
		 g.agregarVertice("Santa Cruz");
		 g.agregarVertice("Tucuman");
		 
		 String [] esperados= {"Cordoba", "Santa Cruz", "Tucuman"};
		 Set<String> obtenidos= g.getCiudades();
		 Assert.iguales(esperados,obtenidos);
	 }
	 
	 
	 
	 
	
	
	
	
	
	
	
	
	
	
	
	
	
}
