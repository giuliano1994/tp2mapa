package usuario;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import negocio.DivisionGrafo;
import negocio.Grafo;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Map.Entry;
import java.awt.event.ActionEvent;

public class DivisionVentana extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static Grafo graf;
	private DivisionGrafo DG;
	private JTable table;
	private Integer tamanio;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DivisionVentana frame = new DivisionVentana(graf);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DivisionVentana(Grafo gr) {
		
		graf = gr;
		tamanio = graf.tamanio() - 1;
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(135, 25, 422, 308);
		contentPane.add(scrollPane);
		
		table = new JTable();
		
		DefaultTableModel model = new DefaultTableModel();
		table.setModel(model);
		
		model.addColumn("Primer Provincia");
		model.addColumn("Segunda Provincia");
		model.addColumn("Similaridad");
	
		scrollPane.setViewportView(table);
		
		
		
		
		
		JButton Finalizar = new JButton("Finalizar");
		Finalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				System.exit(0);
			}
		});
		Finalizar.setBounds(10, 305, 89, 23);
		contentPane.add(Finalizar);
		
		JButton Volver = new JButton("Volver");
		Volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				setVisible(false);  
		
			}
		});
		Volver.setBounds(10, 237, 89, 23);
		contentPane.add(Volver);
		
		
		JButton Generar = new JButton("Generar");
		Generar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JScrollPane scrollPane = new JScrollPane();
				scrollPane.setBounds(135, 25, 422, 308);
				contentPane.add(scrollPane);
				
				table = new JTable();
				
				DefaultTableModel model = new DefaultTableModel();
				table.setModel(model);
				
				model.addColumn("Primer Provincia");
				model.addColumn("Segunda Provincia");
				model.addColumn("Similaridad");
			
				scrollPane.setViewportView(table);
				
				
				
				DG = new DivisionGrafo();
				
				graf = DG.iniciar(graf);
				
				System.out.println(graf);
				
				if (tamanio == 0) {
					
					JOptionPane.showMessageDialog(null, "ya se eliminaron todas las relaciones");
				}
				else {
					
					tamanio--;
			//		System.out.println("tama�o ahora es: " + tamanio);
					
					for (String ver : graf.getCiudades()) {
						
						if (!(graf.recorrerAristas2(ver).isEmpty())) {
							
							for (Entry<String, Integer> ari : graf.recorrerAristas2(ver)) {
								
								//System.out.println("entro si");
								//System.out.println(ver);
								model.addRow(new Object [] {ver , ari.getKey() , ari.getValue()});
							}
				 
						}
						
						else {
						
							System.out.println("entro no");
							System.out.println(ver);
							model.addRow(new Object [] {ver , "nada" , "nada"});
						}
					}
				}
			}
		});
		
		Generar.setBounds(10, 75, 89, 23);
		contentPane.add(Generar);
		
		
		
	}
	
	
	

	

}
