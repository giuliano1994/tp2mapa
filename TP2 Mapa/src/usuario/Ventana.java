package usuario;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import negocio.Grafo;

import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Ventana extends JDialog{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFrame frame;
	static  Grafo gr;
	private int contador;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana window = new Ventana();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
					
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ventana() {
		initialize();
	}

	public void set(JComboBox <Object> choice)
	{
		
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	
	
	
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(200, 200, 600, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		gr = new Grafo ();
		
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(291, 74, 271, 228);
		frame.getContentPane().add(scrollPane);
		
		
		table = new JTable();
		DefaultTableModel model = new DefaultTableModel();
		table.setModel(model);
		
		model.addColumn("Primer Provincia");
		model.addColumn("Segunda Provincia");
		model.addColumn("Similaridad");
		
		scrollPane.setViewportView(table);
		
		
		
	
		String [] provincias=new String []{"Seleccione", "Buenos Aires", "Catamarca", "Chaco", "Chubut", "Cordoba", "Corrientes", 
				"Entre Rios", "Formosa", "Jujuy", "La Pampa", "La Rioja", "Mendoza", "Misiones", "Neuquen",
				"Rio Negro", "Salta", "San Juan", "San Luis", "Santa Cruz", "Santa Fe", "Santiago del Estero",
				"Tierra del Fuego", "Tucuman"};
		
		//LABELS
		JLabel ciudad1 = new JLabel("Elija una provincia");
		ciudad1.setHorizontalAlignment(SwingConstants.CENTER);
		ciudad1.setFont(new Font("Arial", Font.PLAIN, 16));
		ciudad1.setBounds(32, 74, 150, 20);
		frame.getContentPane().add(ciudad1);
				
		JLabel ciudad2 = new JLabel("Elija una provincia");
		ciudad2.setHorizontalAlignment(SwingConstants.CENTER);
		ciudad2.setFont(new Font("Arial", Font.PLAIN, 16));
		ciudad2.setBounds(32, 137, 150, 20);
		frame.getContentPane().add(ciudad2);
				
		JLabel distanciaPeso = new JLabel("Similaridad");
		distanciaPeso.setHorizontalAlignment(SwingConstants.CENTER);
		distanciaPeso.setFont(new Font("Arial", Font.PLAIN, 15));
		distanciaPeso.setBounds(32, 200, 100, 20);
		frame.getContentPane().add(distanciaPeso);
				
		JLabel Titulo = new JLabel("Regionalizaci\u00F3n");
		Titulo.setHorizontalAlignment(SwingConstants.CENTER);
		Titulo.setFont(new Font("Arial", Font.PLAIN, 27));
		Titulo.setBounds(139, 11, 353, 52);
		frame.getContentPane().add(Titulo);
				
		//SELECCIONADORES
		JComboBox<Object> provincia1 = new JComboBox <Object>();
		provincia1.setFont(new Font("Arial", Font.PLAIN, 15));
		provincia1.setBounds(32, 105, 162, 21);
		frame.getContentPane().add(provincia1);
				
		JComboBox<Object> provincia2 = new JComboBox<Object>();
		provincia2.setFont(new Font("Arial", Font.PLAIN, 15));
		provincia2.setBounds(32, 168, 162, 21);
		frame.getContentPane().add(provincia2);
				
		JComboBox<Object> Similaridad = new JComboBox<Object>();
		Similaridad.setFont(new Font("Arial", Font.PLAIN, 15));
		Similaridad.setBounds(32, 231, 100, 22);
		frame.getContentPane().add(Similaridad);
				
				
		provincia1.setModel(new DefaultComboBoxModel<Object>(provincias));
		provincia2.setModel(new DefaultComboBoxModel<Object>(provincias));
				
		Similaridad.setModel(new DefaultComboBoxModel<Object>(new String[]
				{"Seleccione","1","2","3","4","5","6","7","8","9","10"}));
				
		//BOTONES
		JButton botonAgregar = new JButton("Agregar");
		botonAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String prov1 = (String) provincia1.getSelectedItem();
				String prov2 = (String) provincia2.getSelectedItem();
				int rel = Integer.parseInt((String) Similaridad.getSelectedItem());
			
				gr.agregarArista(prov1, prov2, rel);
				contador ++;
				
				
				JScrollPane scrollPane = new JScrollPane();
				scrollPane.setBounds(291, 74, 271, 228);
				frame.getContentPane().add(scrollPane);
				
				
				table = new JTable();
				DefaultTableModel model = new DefaultTableModel();
				table.setModel(model);
				
				model.addColumn("Primer Provincia");
				model.addColumn("Segunda Provincia");
				model.addColumn("Similaridad");
				
				scrollPane.setViewportView(table);
				
				model.addRow(new Object[] {prov1, prov2, Similaridad});
				
			}
		});
		botonAgregar.setFont(new Font("Arial", Font.PLAIN, 15));
		botonAgregar.setBounds(152, 231, 97, 22);
		frame.getContentPane().add(botonAgregar);
		
		JButton ArbolGeneradorMinimo = new JButton("ArbolGeneradorMinimo");
		ArbolGeneradorMinimo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (contador == 0) {
					
					JOptionPane.showMessageDialog(null, "no se ingresaron provincias");
				}
				else {
				
					 new AGMVentana(gr).setVisible(true);
					
				}
				
			}
		});
		ArbolGeneradorMinimo.setFont(new Font("Arial", Font.PLAIN, 15));
		ArbolGeneradorMinimo.setBounds(32, 279, 190, 22);
		frame.getContentPane().add(ArbolGeneradorMinimo);
		
		JButton Finalizar = new JButton("Finalizar");
		Finalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				System.exit(0);
			}
		});
		Finalizar.setFont(new Font("Arial", Font.PLAIN, 15));
		Finalizar.setBounds(464, 318, 97, 22);
		frame.getContentPane().add(Finalizar);
		
		JButton DividirGrafo = new JButton("DividirGrafo");
		DividirGrafo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new DivisionVentana(gr).setVisible(true);
			}
		});
		DividirGrafo.setFont(new Font("Arial", Font.PLAIN, 15));
		DividirGrafo.setBounds(32, 318, 190, 22);
		frame.getContentPane().add(DividirGrafo);
		
		JButton Reiniciar = new JButton("Reiniciar");
		Reiniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Ventana.main(null);
			}
		});
		Reiniciar.setFont(new Font("Arial", Font.PLAIN, 15));
		Reiniciar.setBounds(308, 318, 97, 22);
		frame.getContentPane().add(Reiniciar);
		
		
		
		
		
		
				
	
		
		
	}
}
