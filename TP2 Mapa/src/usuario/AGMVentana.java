package usuario;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import negocio.ArbolGeneradorMinimo;
import negocio.Grafo;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Map.Entry;
import java.awt.event.ActionEvent;
import javax.swing.table.DefaultTableModel;

public class AGMVentana extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static Grafo graf;
	private ArbolGeneradorMinimo ar;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AGMVentana frame = new AGMVentana(graf);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	
	public AGMVentana(Grafo gr) {
		
		graf = gr;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(145, 11, 408, 322);
		contentPane.add(scrollPane);
		
		table = new JTable();
		
		DefaultTableModel model = new DefaultTableModel();
		table.setModel(model);
		
		model.addColumn("Primer Provincia");
		model.addColumn("Segunda Provincia");
		model.addColumn("Similaridad");
		
		
		
		scrollPane.setViewportView(table);
		
		
		
		
		
		
		JButton Generar = new JButton("Generar");
		Generar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				

				JScrollPane scrollPane = new JScrollPane();
				scrollPane.setBounds(145, 11, 408, 322);
				contentPane.add(scrollPane);
				
				table = new JTable();
				DefaultTableModel model = new DefaultTableModel();
				table.setModel(model);
				
				model.addColumn("Primer Provincia");
				model.addColumn("Segunda Provincia");
				model.addColumn("Similaridad");
				
				
				
				scrollPane.setViewportView(table);
				
			
				
				ar = new ArbolGeneradorMinimo(graf);
				
				graf = ar.iniciar();
				
				for (String ver: graf.getCiudades()) {
					
					for (Entry<String, Integer> ari: graf.recorrerAristas2(ver)) {
						
						model.addRow(new Object[] {ver, ari.getKey(), ari.getValue()});
						
					}
				}
			
				
				
			}
		});
		Generar.setBounds(10, 52, 89, 23);
		contentPane.add(Generar);
		
		JButton Volver = new JButton("Volver");
		Volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
							
				setVisible(false);
			}
		});
		Volver.setBounds(10, 208, 89, 23);
		contentPane.add(Volver);
		
		JButton Finalizar = new JButton("Finalizar");
		Finalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				System.exit(0);
			}
		});
		Finalizar.setBounds(10, 255, 89, 23);
		contentPane.add(Finalizar);
		
		JButton Dividir = new JButton("Dividir");
		Dividir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new DivisionVentana(graf).setVisible(true);
			}
		});
		Dividir.setBounds(10, 105, 89, 23);
		contentPane.add(Dividir);
	}
}
