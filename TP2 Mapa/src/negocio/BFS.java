package negocio;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;


public class BFS {

	Grafo bfs;
	private HashMap<String, Boolean> pendientes;
	private HashMap<String, Boolean> marcados;
	Grafo graf;
	private boolean conexo;
	
	public BFS(Grafo g) {
		
		this.pendientes = new HashMap<String, Boolean>();
		this.marcados = new HashMap<String, Boolean>();
		this.graf = g;
		this.bfs = new Grafo();
		
	}
	
	
	
	
	// recorro todos los vertices de un grafo agregandolos a dos listas una de pendientes y otra de marcados
	public boolean recorrerGrafo(String vertice) {
		
		for (String ver : graf.getCiudades()) {
			
			pendientes.put(ver, false);
			marcados.put(ver, false);
		
		}
		// actualizo el vertice pasado como parametro con el valor de True, ahora lo tomamos como pendiente 
		pendientes.replace(vertice, false, true);
		recorrerBFS();
		
		return conexo;
	}
		
		
	
	// recorre la lisa de pendientes y marcados buscando cuales son sus aristas
	private void recorrerBFS() {
		
		while (contadorDePendientes() > 0) {
			// recorro todos los vertices de pendientes cuyo valor sea True
			for (Map.Entry<String, Boolean> ver : pendientes.entrySet()) {
			
				if (ver.getValue()) {
					// recorro todos los vertices de marcados que sean distintos al vertice principal 
					// y su valor sea False, todavia no esta marcado
					for (Map.Entry<String, Boolean> ver2 : marcados.entrySet()) {
						
						if ( ver.getKey().compareTo(ver2.getKey()) != 0 && !(ver2.getValue())) {
						
							for (String ari : graf.vecinosDe(ver.getKey())) {
						
								if ( !(marcados.get(ari))) {
							
									pendientes.replace(ari, false, true);
									// si es una arista del vertice princital y no fue recorrido todavia (marcado = False)
									// lo actalizo en pendiente con un True, ahora lo tenemos que recorrer
								}
							}
						}
					
						pendientes.replace(ver.getKey(), true, false);
						
						marcados.replace(ver.getKey(), false, true);
						// al vertice principal lo quitamos de pendientes (False)
						// y lo agregamos a marcados (True), ya no lo recorremos mas
					}
				}			
			}	
		}	
	
		esConexo(marcados);
	}		
	
		
		
	// comprueba si un grafo es conexo o no conexo
	private boolean esConexo(HashMap<String, Boolean> marcados2) {
		
		conexo = true;
		
		for (Map.Entry<String, Boolean> ver : marcados2.entrySet()) {
			
			if (!ver.getValue()) {
				
				conexo = false;
			}
		}
		return conexo;
	}
		
		
	
	// devuelve el valor numerico del total de vertices cuyo valor es True, dentro de pendientes, cuando es 0 ya los recorri todos 
	private Integer contadorDePendientes() {
		
		int cont = 0;
		
		for (Entry<String, Boolean> ver : pendientes.entrySet()) {
			
			if ((ver.getValue())) {
				
				cont++;
			}
		}
		return cont;
	}
		
		


	@Override
	public String toString() {
		return "BFS [recorridos=" + marcados + "]";
	}




	public HashMap<String, Boolean> getPendientes() {
		return pendientes;
	}




	public void setPendientes(HashMap<String, Boolean> pendientes) {
		this.pendientes = pendientes;
	}




	public HashMap<String, Boolean> getMarcados() {
		return marcados;
	}




	public void setMarcados(HashMap<String, Boolean> marcados) {
		this.marcados = marcados;
	}
		
		
	
	
	
	
}
