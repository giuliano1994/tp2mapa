package negocio;


public class DivisionGrafo {
	
	private Grafo graf;
	private String verticeMasPesado;
	private String aristaMasPesada;
	private Integer pesoMasPesado;
	
	
	
	
	public DivisionGrafo () {
		
		pesoMasPesado = 0;
		
	}
	
	
	
	// recibe un grafo como parametro y llama arecorrer grafo
	public Grafo iniciar(Grafo g) {
		
		if ( g == null) {
			
			throw new IllegalArgumentException("no se puede eliminar una relacion de un grafo nulo");
		}
		
		graf = g;
		
		recorrerGrafo();
		
		return graf;
	}


	
	
	// recorre los vertices y aristas coparando sus pesos y tomando en cada iteracion el de valor mas alto
	public void recorrerGrafo() {
		
		for (String ver : graf.getCiudades()) {
			
			for (String ari : graf.vecinosDe(ver)) {
				
				if (graf.getSimilaridad(ver, ari) >= pesoMasPesado) {
					
					verticeMasPesado = ver;
					aristaMasPesada = ari;
					pesoMasPesado = graf.getSimilaridad(ver, ari);	
				}
			}
		}
		
		eliminarRelacion();
	}
	
	
	// elimina una relacion entre un vertice y la arista mas pesada
	private void eliminarRelacion() {
		
		graf.eliminarArista(verticeMasPesado, aristaMasPesada);
		
	}





	
	
	
	@Override
	public String toString() {
		return "DivicionGrafo [graf=" + graf + ", verticeMasPesado=" + verticeMasPesado + ", aristaMasPesada="
				+ aristaMasPesada + ", pesoMasPesado=" + pesoMasPesado + "]";
	}





	public Grafo getGraf() {
		return graf;
	}





	public void setGraf(Grafo graf) {
		this.graf = graf;
	}





	public String getVerticeMasPesado() {
		return verticeMasPesado;
	}





	public void setVerticeMasPesado(String verticeMasPesado) {
		this.verticeMasPesado = verticeMasPesado;
	}





	public String getAristaMasPesada() {
		return aristaMasPesada;
	}





	public void setAristaMasPesada(String aristaMasPesada) {
		this.aristaMasPesada = aristaMasPesada;
	}





	public Integer getPesoMasPesado() {
		return pesoMasPesado;
	}





	public void setPesoMasPesado(Integer pesoMasPesado) {
		this.pesoMasPesado = pesoMasPesado;
	}
	
	
	
	
	
	
	

}
