package negocio;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class ArbolGeneradorMinimo  {
	
	Grafo arbol;
	private Map<String, Boolean> verticesRecorridos;
	private Integer mejorPeso;
	private String mejorProvincia;
	private String verticeActual;
	Grafo graf;
	private BFS bfs;
	
	
	public ArbolGeneradorMinimo(Grafo g) {
		
		this.verticesRecorridos = new HashMap<String, Boolean>();
		this.graf = g;
		this.arbol = new Grafo();
		this.bfs = new BFS(g);
	
	}
	
	
	
	public Grafo iniciar () {
		
		Iterator<String> it = graf.getCiudades().iterator();
		
		String ciudad = it.next();
		
		if (!(bfs.recorrerGrafo(ciudad))) {
			
			throw new IllegalArgumentException("el grafo no es conexo, no se puede aplicar bfs");
		}
		else {
			
		
			if (graf.getCiudades().size() > 0) {
				
				verticeActual = it.next();
				
				recorrerGrafo();
			}
			
			return arbol;
		}
	}
		
	
	
	
	
	
		
		
	// se recorre todos los vertices y se los agrega a un map con valor False
	private void recorrerGrafo() {
			
			for (String ver : graf.getCiudades()) {
				
				verticesRecorridos.put(ver, false);
			}
				// actualizamos el valor de un vertice a True
			verticesRecorridos.replace(verticeActual, false, true);

			compararRelacionesDeAristas();
		}

	
		
	// recorremos los vertices buscando todas las aristas mas livianas 
	private void compararRelacionesDeAristas() {
		
		while (arbol.tamanio() < graf.tamanio()) {
					
			mejorPeso = Integer.MAX_VALUE;
			mejorProvincia = "";
			// recorremos todos los vertices cuyos calores sean True
			for (Map.Entry<String, Boolean> ver : verticesRecorridos.entrySet()) {
			
				if (ver.getValue()) {
					// recorremos todas las aristas cuyos valores sean False
					for (Map.Entry<String, Boolean> ari : verticesRecorridos.entrySet()) {
		
						if (!(ari.getValue())) {
								
							if (graf.existeArista(ver.getKey(), ari.getKey())) {
								
								if (graf.getSimilaridad(ver.getKey(), ari.getKey()) < mejorPeso) {
										
									mejorPeso = graf.getSimilaridad(ver.getKey(), ari.getKey());
									mejorProvincia = ari.getKey();
									verticeActual = ver.getKey();
									// actualizo los datos con los que corresponden a la arista mas barata
								}
							}
						}
					}		
				}
			}
			
			agregarVerticeArbol();
		}
		
	}
		
		
		
	// agrega un vertice a un nuevo grafo para luego devolverlo 
	private void agregarVerticeArbol() {

		arbol.agregarArista(verticeActual, mejorProvincia, mejorPeso);
		
		verticesRecorridos.replace(mejorProvincia, false, true);
	}
		
	
		

	
	
	@Override
	public String toString() {
		return "ArbolGeneradorMinimo [arbol=" + arbol + "]";
	}



	public Grafo getArbol() {
		return arbol;
	}



	public void setArbol(Grafo arbol) {
		this.arbol = arbol;
	}



	public Map<String, Boolean> getVerticesRecorridos() {
		return verticesRecorridos;
	}



	public void setVerticesRecorridos(Map<String, Boolean> verticesRecorridos) {
		this.verticesRecorridos = verticesRecorridos;
	}



	public Integer getMejorPeso() {
		return mejorPeso;
	}



	public void setMejorPeso(Integer mejorPeso) {
		this.mejorPeso = mejorPeso;
	}



	public String getMejorProvincia() {
		return mejorProvincia;
	}



	public void setMejorProvincia(String mejorProvincia) {
		this.mejorProvincia = mejorProvincia;
	}



	public String getVerticeActual() {
		return verticeActual;
	}



	public void setVerticeActual(String verticeActual) {
		this.verticeActual = verticeActual;
	}



	public Grafo getGraf() {
		return graf;
	}



	public void setGraf(Grafo graf) {
		this.graf = graf;
	}
	
	
	

	
	
	
}
