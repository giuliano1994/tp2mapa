package negocio;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;


public class Grafo {
	
	private HashMap<String, HashMap<String, Integer>> mapa;
	
	public Grafo ()
	{
		this.mapa = new HashMap<String, HashMap<String, Integer>>();
	}
	
	
	
	// agrega una ciudad al mapa
	public void agregarVertice(String ciudad) 
	{
		mapa.put(ciudad, new HashMap<>());

	}
	
	
	
	// agrega a una ciudad un vecino y el peso de su arista
	//en la ciudad origen agrega destino y similaridad
	//en la ciudad destino agrega origen y similaridad
	public void agregarArista(String origen, String destino, Integer similaridad) 
	{
		verificarLoop(origen, destino);
		
		if (!mapa.containsKey(origen)) {
			
			agregarVertice(origen);
		}
		if (!mapa.containsKey(destino)) {
			
			agregarVertice(destino);
		}
		
		mapa.get(origen).put(destino, similaridad);
		mapa.get(destino).put(origen, similaridad);

	}
	

	
	// verifica loop entre 2 ciudades
	public void verificarLoop(String vertice, String adyacentes) 
	{
		if (vertice.compareTo(adyacentes) == 0) {
			throw new IllegalArgumentException
			("La ciudad de origen no puede ser igual a la ciudad de destino");
		}
	}
	
	
	
	// elimina de la lista de vecinos de origen a destino
	// y elimina de destino a origen
	public void eliminarArista(String origen, String destino) 
	{
		if (existeArista(origen, destino))
		{
			mapa.get(origen).remove(destino);
			mapa.get(destino).remove(origen);
		}
	//	else
//			throw new IllegalArgumentException("La arista entre "+ origen + " y "+ destino+ " no existe");
	}
	
	
	
	// elimina ciudad del mapa 
	//y a su vez elimina esta ciudad de sus anteriores vecinos
	public void eliminarVertice (String estaCiudad) 
	{
		if (existeVertice(estaCiudad))
		{
			for(String vecino: vecinosDe(estaCiudad))
			{
				eliminarArista(vecino, estaCiudad);
			}
			mapa.remove(estaCiudad);
		}
		else
			throw new NullPointerException("La ciudad que quiere eliminar no existe");
	}
	
	
	
	// verifica si existe la arista entre origen y destino
	public boolean existeArista(String origen, String destino) 
	{
		return mapa.containsKey(origen) && mapa.get(origen).containsKey(destino);
	}


	
	// verifica que el mapa contenga a la ciudad
	public boolean existeVertice(String ciudad)
	{
		return mapa.containsKey(ciudad);	
	}
	
	
	
	// devuelve la similaridad entre origen y destino
	public Integer getSimilaridad(String origen, String destino)
	{
		if (mapa.containsKey(origen) && existeArista(origen, destino))
			return mapa.get(origen).get(destino);
		
		return null;
	}

	

	// devuelve la cantidad de ciudades del mapa
	public int tamanio() 
	{
		return mapa.size();
	}
	
	
	// genera un numero aleatorio y devuelve el vertice que corresponde ese numero
	public String verticeAleatorio() {
		
		String resultado = null;
		
		int random;
		
		Set<String> vertices = new HashSet<String>();
		
		vertices = getCiudades();
		
		random = new Random().nextInt(vertices.size());
		
		int i = 0;
		
		for (String ver : vertices) {
			
			if  (i == random) {
				
				resultado = ver;
			}
			
			i++;
		}

		return resultado;
	}
	
	
	// devuelve un Set <String: provincia_2, Integer: relacion> con todas las provincias que tienen relacion con provincia_1 
	public Set<Entry<String, Integer>> recorrerAristas2 (String vertice) {
		
		if (existeVertice(vertice)) {
			
			return mapa.get(vertice).entrySet();
		}
		
		return null;
	}
	
	
	
	// devuelve un conjunto con todas las ciudades del mapa
	public Set<String> getCiudades() 
	{
			return mapa.keySet();
	}
	
	

	// devuelve un conjunto con todas las ciudades vecinas de estaCiudad
	public Set<String> vecinosDe(String estaCiudad) 
	{
		if (mapa.containsKey(estaCiudad))
			return mapa.get(estaCiudad).keySet();
		
		return null;
	}
	
	
	
	@Override
	public String toString() {
		return "Grafo [vecino=" + mapa + "]";
	}




	public void setVecino(HashMap<String, HashMap<String, Integer>> vecino) {
		this.mapa = vecino;
	}
	
	
	
	
	
	

	
}
